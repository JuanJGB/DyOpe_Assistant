package com.projeto.repository;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;

import com.projeto.models.Peca;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PecaRepository extends CrudRepository<Peca, Serializable> {
	
	Peca findById (long id);
	
	Peca findByDescricao (String descricao);

}
